namespace FirstPersonCharacterController
{
	public interface ICursorInteractable
	{
		void OnUse();
	}
}
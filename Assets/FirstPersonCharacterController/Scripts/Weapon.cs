using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonCharacterController {

    public class Weapon : HoldableItem
    {
        [SerializeField] private GameObject bulletPrefab; //The used bullet type
        [SerializeField] private Transform bulletSpawnPoint; //The tip of the gun model
        [SerializeField] private bool autoFire; //Allows continuous shooting on button hold if set to true
        [SerializeField] private bool autoReload; //Calls reload when ammo reaches zero
        [SerializeField] private int ammoCapacity;
        [SerializeField] private float reloadTime;
        [SerializeField] private float shootForce; //Force added to the the bullet object
        [SerializeField] private float spread; //The inaccuracy of the gun
        [SerializeField] private float timeBetweenShots;

        [Header("Optional")]
        [SerializeField] private ParticleSystem gunFlashEffect;
        [SerializeField] private Text ammoCounter;
        [SerializeField] private Animator animator;

        private PlayerInput playerInput;
        private Camera playerCamera;
        private int bulletsLeft;
        private List<GameObject> pooledBullets;
        private float shootingNotAllowedTimer;
        

        protected override void Start()
        {
            base.Start();

            Assert.IsNotNull(bulletSpawnPoint);
            Assert.IsNotNull(bulletPrefab);

            playerInput = FindObjectOfType<PlayerInput>();
            playerCamera = Camera.main;
            bulletsLeft = ammoCapacity;
        }

        public override void PickUp(string inHandLayerName)
        {
            // Normally pick up the weapon item
            base.PickUp(inHandLayerName);

            // Instantiate bullet pool of ammo capacity size
            shootingNotAllowedTimer = 0;
            pooledBullets = new List<GameObject>();
            for (int i = 0; i < ammoCapacity; i++)
            {
                GameObject bullet = Instantiate(bulletPrefab);
                bullet.SetActive(false);
                pooledBullets.Add(bullet);
            }
            // Optionaly use UI for displaying ammo count
            if (ammoCounter != null)
            {
                ammoCounter.gameObject.SetActive(true);
                UpdateAmmoUI();
            }
            if (animator)
            {
                animator.SetBool("Reloading", false);
                animator.enabled = true;
            }
        }

        public override void Drop()
        {
            // Normally drop the weapon item
            base.Drop();

            // Destroy bullet pool
            foreach (var bullet in pooledBullets)
            {
                Destroy(bullet);
            }
            pooledBullets = null;
            // Disable ammo UI
            if (ammoCounter != null)
            {
                ammoCounter.gameObject.SetActive(false);
            }
            if (animator)
            {
                animator.SetBool("Reloading", false);
                animator.enabled = false;
            }
        }

        private void Update()
        {
            if (IsHeld)
            {
                // Wait until shooting allowed
                if (shootingNotAllowedTimer > 0)
                {
                    shootingNotAllowedTimer -= Time.deltaTime;
                    return;
                }


                // Auto reload
                if(bulletsLeft == 0 && autoReload)
                {
                    StartCoroutine(Reload());
                    return;
                }

                // Shooting
                if (bulletsLeft > 0)
                {
                    if (playerInput.GetButtonDown("Fire") || (autoFire && playerInput.GetButton("Fire")))
                    {
                        Shoot();
                        shootingNotAllowedTimer = timeBetweenShots;
                    }
                }

                // Realoading
                if (Input.GetKeyDown(KeyCode.R))
                {                    
                    StartCoroutine(Reload());
                }
            }
        }

        private IEnumerator Reload()
        {
            shootingNotAllowedTimer = reloadTime;
            UpdateAmmoUI();
            if (animator) animator.SetBool("Reloading", true);

            yield return new WaitForSeconds(reloadTime);

            bulletsLeft = ammoCapacity;
            UpdateAmmoUI();
            if (animator) animator.SetBool("Reloading", false);
        }

        private void Shoot()
        {
            GameObject bullet = GetPooledBullet();
            if (bullet != null)
            {
                // Spawn a bullet
                bullet.transform.SetPositionAndRotation(bulletSpawnPoint.position, bulletPrefab.transform.rotation);
                bullet.SetActive(true);

                // Flash
                if (gunFlashEffect != null)
                {
                    gunFlashEffect.Play();
                }

                // Compute shooting direction
                Ray forwardRay = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                Vector3 targetPoint = forwardRay.GetPoint(100);
                if (Physics.Raycast(forwardRay, out RaycastHit hit))
                    targetPoint = hit.point;
                Vector3 direction = targetPoint - bulletSpawnPoint.position;
                direction += new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), 0);

                // Shoot
                bullet.transform.up = direction.normalized;
                if (bullet.TryGetComponent<Rigidbody>(out Rigidbody r))
                {
                    r.AddForce(direction.normalized * shootForce, ForceMode.Impulse);
                }

                bulletsLeft--;
                UpdateAmmoUI();
            }
        }

        private GameObject GetPooledBullet()
        {
            //pick bullet from pre-instantiated pool
            for (int i = 0; i < pooledBullets.Count; i++)
            { 
                if (!pooledBullets[i].activeInHierarchy)
                {
                    return pooledBullets[i];
                }
            }
            //instantiate a new bullet if there is not enough of them
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.SetActive(false);
            pooledBullets.Add(bullet);
            return pooledBullets[pooledBullets.Count - 1];
        }

        private void UpdateAmmoUI()
        {
            if (ammoCounter != null)
            {
                ammoCounter.text = "ammo: " + bulletsLeft;
            }
        }
    }
}

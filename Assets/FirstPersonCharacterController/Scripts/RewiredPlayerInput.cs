#if REWIRED
using Rewired;

namespace FirstPersonCharacterController
{
	public class RewiredPlayerInput : PlayerInput
	{
		private Player player;

		private void Start()
		{
			player = ReInput.players.GetPlayer(0);
		}

		public override bool GetButtonDown(string actionName)
		{
			return player.GetButton(actionName);
		}
		
		public override bool GetButton(string actionName)
		{
			return player.GetButton(actionName);
		}

		public override float GetAxis(string actionName)
		{
			return player.GetAxis(actionName);
		}

		
	}
}
#endif

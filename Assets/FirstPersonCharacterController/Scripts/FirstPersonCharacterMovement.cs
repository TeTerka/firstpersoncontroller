using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPersonCharacterController
{
	public class FirstPersonCharacterMovement : MonoBehaviour
	{
		[SerializeField] private PlayerInput playerInput; 
		[SerializeField] private CharacterController characterController;
		[SerializeField] private FirstPersonCharacterSettings characterSettings;
		[SerializeField] private Transform groundCheck;
		[SerializeField] private Transform ceilingCheck;
		[SerializeField] private LayerMask groundMask;

		private Vector3 velocity;
		private bool isGrounded = true;
		private bool wasGrounded = true;
		private bool isCrouching;
		private bool isTouchingCeiling;
		private float originalHeight, targetHeight;
		private RaycastHit[] raycastHits = new RaycastHit[5];
		private float speedMultiplier = 1f;
		private float targetSpeedMultiplier;
		private Vector3 fallPosition;

		public float SpeedMultiplier => speedMultiplier;
		public float TargetSpeedMultiplier => targetSpeedMultiplier;
		public bool IsCrouching => isCrouching;

		public event Action<float> Fall; // Fall height

		private void Awake()
		{
			Assert.IsNotNull(characterController);
			Assert.IsNotNull(characterSettings);
			Assert.IsNotNull(groundCheck);
			Assert.IsNotNull(ceilingCheck);

			originalHeight = targetHeight = characterController.height;
		}

		private void Update()
		{
			ceilingCheck.transform.localPosition = new Vector3(0, characterController.height, 0);
			
			isGrounded = Physics.CheckSphere(groundCheck.position,0.1f, groundMask);
			isTouchingCeiling = Physics.CheckSphere(ceilingCheck.position, 0.2f, groundMask);
			isCrouching = isGrounded && playerInput.GetButton(PlayerInput.Action.Crouch);
			targetSpeedMultiplier = 1f;
			
			// Gravity and ceiling
			if (isGrounded && velocity.y < 0)
				velocity.y = -2f;
			if (isTouchingCeiling && velocity.y > 0)
				velocity.y = 0;

			// Basic movement
			float moveX = playerInput.GetAxis(PlayerInput.Action.MoveX);
			float moveZ = playerInput.GetAxis(PlayerInput.Action.MoveZ);
			Vector3 move = transform.right * moveX + transform.forward * moveZ;

			if (playerInput.GetButton(PlayerInput.Action.Sprint))
				targetSpeedMultiplier *= 1.5f;

			// Crouching
			targetHeight = isCrouching ? 1f : originalHeight;
			if(isCrouching)
				targetSpeedMultiplier *= 0.5f;
			characterController.height = Mathf.Lerp(characterController.height, targetHeight, 0.2f * 60f * Time.deltaTime);
			characterController.center = new Vector3(0, characterController.height * 0.5f, 0);
			
			// Jumpings
			if (isGrounded && !isCrouching && playerInput.GetButtonDown(PlayerInput.Action.Jump))
			{
				var maxHeight = characterSettings.jumpHeight;
				var hitCount = Physics.RaycastNonAlloc(ceilingCheck.position, ceilingCheck.up, raycastHits, characterSettings.jumpHeight, groundMask);
				if (hitCount > 0)
					maxHeight = raycastHits[0].point.y - ceilingCheck.position.y;
				
				velocity.y = Mathf.Sqrt(maxHeight * -2f * characterSettings.gravity);
			}
			
			velocity.y += characterSettings.gravity * Time.deltaTime;
			characterController.Move(velocity * Time.deltaTime);
			
			// Apply basic movement
			speedMultiplier = Mathf.Lerp(speedMultiplier, targetSpeedMultiplier, 0.2f * 60f * Time.deltaTime);
			characterController.Move(move * (characterSettings.moveSpeed * speedMultiplier * Time.deltaTime * 60f));
			
			// Falling event
			if (!wasGrounded && isGrounded)
				Fall?.Invoke(fallPosition.y - transform.position.y);

			if (!isGrounded && transform.position.y > fallPosition.y || isGrounded)
				fallPosition = transform.position;

			wasGrounded = isGrounded;
		}
	}
}

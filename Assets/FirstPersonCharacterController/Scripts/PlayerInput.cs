using UnityEngine;

namespace FirstPersonCharacterController
{
	public abstract class PlayerInput : MonoBehaviour
	{
		// This is an abstract layer to use different input options
		// For Unity input, strings need to be updated in Unity input settings
		// You can also use Rewired plugin with RewiredPlayerInput and setup actions in RewiredInputManager
		// Or create any other PlayerInput class of your own
		public static class Action
		{
			public const string MoveX = "Horizontal";
			public const string MoveZ = "Vertical";
			public const string Sprint = "Sprint";
			public const string Crouch = "Crouch";
			public const string Jump = "Jump";
			public const string LookHorizontal = "Mouse X";
			public const string LookVertical = "Mouse Y";
			public const string Use = "Use";
			public const string Zoom = "Zoom";
			public const string Fire = "Fire";
		}
		
		public abstract bool GetButtonDown(string actionName);
		public abstract bool GetButton(string actionName);
		public abstract float GetAxis(string actionName);
	}
}

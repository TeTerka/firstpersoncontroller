
using UnityEngine;

namespace FirstPersonCharacterController
{
	public class UnityPlayerInput : PlayerInput
	{
		public override bool GetButtonDown(string actionName)
		{
			return Input.GetButtonDown(actionName);
		}
		
		public override bool GetButton(string actionName)
		{
			return Input.GetButton(actionName);
		}

		public override float GetAxis(string actionName)
		{
			return Input.GetAxis(actionName);
		}
	}
}

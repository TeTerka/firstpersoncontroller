using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FirstPersonCharacterController
{
    public class DamagableObject : MonoBehaviour
    {
        [SerializeField] private float health;

        public void DamageSelf(float damage)
        {
            health -= damage;
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
